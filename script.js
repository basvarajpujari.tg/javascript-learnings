function test() {
    var foo = 33;
    if (foo) {
        // It will give throw ReferenceError
    //   let foo = foo + 55; 
    }
  }
  test();
  

// const

// It will give throw Error, (Cannot declare constant with the same name as a function )
function f() {
}

// const f = 20;

// Conversion (String --> Numbers), parseInt

let value = '23.01';

console.log(parseInt(value));

// parseFloat

function areaOfCircle(radius) {
    return 2.0 * parseFloat(radius) * Math.PI;
}

console.log(areaOfCircle(17.22));