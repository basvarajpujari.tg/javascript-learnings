const numbers = [10, 20, 30, 40, 50, 60];

// for

// for (let i = 0; i < numbers.length; i++) {
//     console.log(numbers[i]);
// }

// do...while() loop ( it iterates at least once, even if cdtn is not satisfying)
// (known as exit-controlled loop, bcz loop's condition is after before exceution of code)

let count = 1;

// do{
//     console.log(`count- ${count}`);
//     count++;
// } while (count <=5)

// while loop, (known as entry-controlled loop, bcz loop's condition is checked before exceution of code)
// ( it iterates not even once, even if cdtn is not satisfying)

let value = 1;

// while(value < 5){
//     console.log(`value- ${value}`);
//     value++;
// }

// label

let sum = 0;
a = 1;

/* outerloop: while (true) {
  a = 1; // Reset variable a to 1 at the beginning of each iteration

  innerloop: while (a < 3) {
    sum += a;
    if (sum > 12) {
      break outerloop;
    }

    console.log(`sum- ${sum}`);
    a++;
  }
} */


//for..of with break

const fruits = ['apple', 'banana', 'cherry', 'date', 'elderberry'];

/* for ( const fruit of fruits ){
    console.log(fruit);
    if (fruit === 'banana') {
        console.log('Found banana! Exiting the loop!!');
        // break statement used to terminate a loop immediately after condtn gets 
        break; 
    }
     console.log(fruit);
} */

console.log('/***************** continue **********************/');

// for..in with continue

/* for( const index in fruits ){
    const fruit = fruits[index];
    if (fruit === 'banana') {
        console.log('Found banana! Exiting the loop!!');
        // continue statement  it terminates the current iteration of the loop and continue with further iterations
        continue; 
    }
     console.log(fruit);
} */


// for...in loop with object(Student)

const Student = {
    name: 'Bhavyansh',
    age: 4,
    class : 'Pre-Primary',
    greet(){
        console.log(`Hello ${this.name} welcome to ${this.class} class`);
    }
}

/*
for( const key in Student ){
    if (typeof Student[key] === 'function') {
        Student[key]();
    } else{
        console.log(`${key} : ${Student[key]}`);
    }
} 
*/

// Student.greet();


// for..of loop with object(Student)

const Teacher = {
    name: 'VIRAT',
    age: 34,
    class : 'Modern Day Great',
    greet(){
        console.log(`Hello ${this.name} welcome to ${this.class} class`);
    }
}

/**
 * In for...of loop ,the loop variable holds the value of the current element, not the  key or index.
 * Therefore, to call a function inside for...of loop we need to call it with a specific context using .call() method
 */
for (const [key, value] of Object.entries(Teacher)) {
    if (typeof value === 'function') {
        value.call(Teacher);
    } else{
        console.log(`${key} : ${value}`);
    }
}