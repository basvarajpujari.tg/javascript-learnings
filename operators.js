// Assignment operators

// Assigning values to num1, num2 variables

let num1 = 20;
let num2 = 20;

// Remainder assignment, computes the remainder of dividing the left operand by the right operand and assigns the result to the left operand.

let a = 25;

// console.log(a%9);

// Comparison operators

// console.log(10 === '10');

// console.log(9!=8);

// console.log('raghu' === 'ram');

// console.log(90>20);

// console.log(10<20);


// epxlain bitwise opeartor in js,(bitwise operators manipulate the binary representations of numbers at the bit level. They treat their operands as a sequence of 32 bits (zeros and ones) rather than as decimal, hexadecimal, or octal numbers.)

// console.log(5 & 3);

// console.log(5 | 3);

// console.log(10 << 1);

// console.log(10 >> 1);

// ternary operator, (also known as the conditional operator) is a concise way to write conditional expressions. It takes three operands and evaluates a condition, returning one of two expressions based on whether the condition is true or false. 

const isUserLoggedIn = true;

const greeting = isUserLoggedIn ? "Namaskara !!!" : "Please log in to continue";

console.log(greeting);

// Relational operators 

// in operator

const Person = {
    name: 'mr das',
    age: 32,
    city: 'Bharat'
};

console.log('name' in Person);

// instanceOf

function Animal(name) {
    this.name = name;
}

function Dog(name, breed) {
    Animal.call(this, name)
    this.breed = breed;
}

const myDog  = new Dog('zuvi', 'golden')


console.log("is Dog instanceof :-",myDog instanceof Dog); // true
console.log("is Dog instanceof :-",myDog instanceof Animal); // false
console.log("is Dog instanceof :-",myDog instanceof Object); // Output: true (myDog is an instance of Object, since all objects inherit from Object)