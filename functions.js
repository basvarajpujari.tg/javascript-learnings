// functions

// Function Declaration

function grret() {
  console.log("Namaste!!!");
}

// Function Expression

const sum = function add(a, b) {
  return a + b;
};

// console.log(sum(10, 20));

// function hoisting (Function hoisting is a behavior in JavaScript where function declarations are moved to the top of their containing scope,regardless of where the function is declared in the code. This allows us to call a function before it appears in the code without causing a ReferenceError.)

// Function hoisting only works with function declarations — not with function expressions.

sayNamaste("raghu");

function sayNamaste(name) {
//   console.log(`namaste ${name}`);
}


// recursive function (A recursive function in JavaScript is a function that calls itself until a specific condition is met, allowing for the repetitive execution of code. )

// factorial 

function factorial(num) {
    //
    if (num === 0 || num === 1 ) {
        return 1;
    }
    
    else{
        return num * factorial(num - 1);
    }
}

// console.log(factorial(5))

// closure (closure in JavaScript is a function that has access to the variables and parameters of its outer (enclosing) function, even after the outer function has finished executing. )

// here calculator is formed closure with functions add, sub ( we can access result variable value inside the functions )

function calculator() {
    let result = 0;

    return{
        add : function (num) {
            result +=num;
            return result
        },
        sub : function (num) {
            result -=num;
            return result
        }
    }
}

const transact = calculator()

// console.log(transact.add(90));
// console.log(transact.sub(50));

// arguments

function myJoin(separator) {
    let value ='';

    for (let i = 0; i < separator.length; i++) {
        value += separator[i]; 
        if (i !== separator.length - 1) {
            value += " ";
        }
    }

    return value;
}

// console.log(myJoin(['king', 'rahul', 'prasad', 'shrinath']));

// arrow functions 

// code is working

// function Insaan() {
//     // const self  = this;
//     let age = 50

//     setInterval(() => {
//         age = age+1;
//         console.log(`age-${age}`);
//     }, 1000);
// }

// Insaan();

// code is not working

// function Insaan() {
//     this.age = 50;

//     setInterval(() => {
//         this.age++;
//     }, 1000);
// }

// const person = new Insaan();
// console.log(person);