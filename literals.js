// Literals present values in js

/**
 * Array Literals
 * Boolean Literals (true and false)
 * Numeric Literals (Integer Values, Flocating-point values, )
 * Object Literals  ()
 * RegExp Literals
 * String Literals
 * Template Literals
 */

// Array Literals --> It is a list of zero or more expressions, enclosed in square brackets ([]).

const heroes = [, , /* empty-value */ "hatim", "noddy", , "shaktiman", , ,];
console.log(heroes);
// console.log(heroes.length);

//  Object Literals --> is a list of zero or more pairs of property names and associated values of an object, enclosed in curly braces ({}).

const Person = {
  name: "raju",
  age: 19,
  lorem: "demo",
};

// console.log(Person.lorem);

// RegExp Literals  --> It is a pattern enclosed between slashes. The following is an example of a regex literal.

const numberRegex = "^-?d+(.d+)?$"; // regex for number

// Template literals are enclosed by the back-tick (` I'm Template literal `) (grave accent) character instead of double or single quotes.

// Escaping characters

const quote = '"Honesty is The Best Policy "';

// console.log(quote);

//

// if (condition) var num = 20;

const b = false;
if (b) {
  // this condition evaluates to true
}
if (b == true) {
  // this condition evaluates to false
}

// for await...of loop (used to iterate over asynchronous iterable objects, such as those returned by asynchronous generator functions)

// asynchronous function
async function delay(ms){
  return new Promise(resolve => setTimeout(resolve, ms));
}

// asynchronous generator function
async function* asyncNumbers() {

  let count = 1;
  while (count <=5) {
    yield await delay(1000);

    yield count++; // Yield the current count and then increment it
  }
}

//  asynchronous  function
async function consumeAsyncNumbers(){
  for await(const num of asyncNumbers()){
    console.log(num);
  }
}

//
consumeAsyncNumbers();
